#!/usr/bin/env python
#-*- coding:utf-8 -*-

import Image
import glob
files = glob.glob("./entrada/*.jpg")

for pic in files:
    im = Image.open(pic)
    im2 = Image.open(pic)
    
    outPath = "./saida/"
    outName = pic.split('/')[-1].split('.jpg')[0]
    outfile1 = outPath + outName + '-a.jpg'
    outfile2 = outPath + outName + '-b.jpg'
    
    total = im.size
    metade = im.size[0]/2
    largura = im.size[0]
    altura = im.size[1]
    
    #left, upper, right, and lower
    
    print "-------------------------"
    print "-------> Loading: " + outName + ".jpg"
    print "-------> Checing ..."
    
    path1 = (0,0,metade,altura)
    path2 = (metade +1, 0, largura, altura)
    size = [metade, altura]
    
    print "-------> Creating " + outfile1
    im = im.crop(path1)
    im.thumbnail(size, Image.ANTIALIAS)
    im.save(outfile1, "JPEG")
    
    print "-------> Creating " + outfile2
    im2 = im2.crop(path2)
    im2.thumbnail(size, Image.ANTIALIAS)
    im2.save(outfile2, "JPEG")

print "###################################"
print "### It's Done Mr. Photo ###"
print "###################################"
raw_input('')