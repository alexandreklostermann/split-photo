Autor: Alexandre Martins Klostermann
Email: alexandre@imotion-info.com

Descrição: Este é um script simples que tem a função de ler uma determinada pasta de arquivos pegar todas as imagens dela e em seguida salvar estas imagens divididas ao meio em outra pasta.
A premissa é não alterar a resolução nem DPI das imagens. Ideal para fotografos que criam a arte de uma página de album no photoshop e precissam separa-las no meio de modo a imprimir uma imagem em duas páginas do album.